# Analysis of the results of network reconstruction with GRN variants

The scripts for analysis of the results of network reconstruction are in the folder analysis. The subfolders "steady-state" and "time-series" correspond to the analysis of the results obtained from steady-state and time-series data, respectively.

In each folder, the main source file is analysis.R, which can be started in any environment that supports the use of R environment for statistical computing. In particular, from command-line interface on Unix-like systems, one can use the command

`R --no-save < analysis.R > analysis.log`

to start the script and obtain the graphs from the article section "Results". Many graphs are being produced the scripts, in addition to intermediate ones that have been used for checking the script correctness, the ones included in the article are the ones with the prefixes "bp-assoc" and "bp-score".

To run analysis.R in the folder "steady-state", the following files are needed as input:

*	nr-ss-ds-nets.csv
*	nr-ss-methods.csv
*	nr-ss-nets.csv
*	nr-ss-ds.csv
*	nr-ss-metrics.csv
*	nr-ss-results.csv

Similarly, to run analysis.R in the folder "time-series", the following files are needed as input:

*	nr-methods.csv
*	nr-nets.csv
*	nr-ts-nets.csv
*	nr-metrics.csv
*	nr-results.csv
*	nr-ts.csv
